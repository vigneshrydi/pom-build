package week9.day9;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import freemarker.template.utility.NullArgumentException;
//@Test
public   class ReadExcelold {
	public static Object[][] readExcel( String dataSheetName)throws IOException {
		XSSFWorkbook wb = new XSSFWorkbook("./excel/"+dataSheetName+".xlsx");
		XSSFSheet sheet = wb.getSheetAt(0);   //go to sheet
		int rowCount = sheet.getLastRowNum(); //rowcount
		System.out.println(rowCount);
		short cellCount = sheet.getRow(0).getLastCellNum();  //columncount
		System.out.println(cellCount);
		Object[][] data= new Object[rowCount][cellCount];
		for(int i=1;i<=rowCount;i++) {
			XSSFRow row = sheet.getRow(i);     // go to specific row
			for(int j=0;j<cellCount;j++) {
				XSSFCell cell = row.getCell(j);
				try {
//					XSSFRichTextString value = cell.getRichStringCellValue();
//					System.out.println(value);
					data[i-1][j]=cell.getStringCellValue();
				}catch(NullArgumentException e) {
				e.printStackTrace();
				}
				
			}
			
		}
		return data;
		
	}

}
