package wdMethods;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class ZoomCarApplication extends SeMethods {
	@Test
	public void zoomCar() {
		try {
			startApp("chrome", "https://www.zoomcar.com/chennai");
			WebElement eleLogin =  locateElement("class","search");
			click (eleLogin);
			Thread.sleep(3000);

			WebElement eleLogin1 =locateElement("xpath","//div[@class='component-popular-locations']/div[2]");
			click(eleLogin1);
			WebElement eleLogin2 =locateElement("xpath","(//button[@class='proceed'])");
			click(eleLogin2);
			WebElement eleLogin3 =locateElement("xpath","(//div[@class='days']/div[2])");
			click(eleLogin3);

			WebElement eleLogin5 =locateElement("xpath","(//button[@class='proceed'])");
			click(eleLogin5);
			WebElement eleLogin6 =locateElement("xpath","//button[contains(text(),'Done')]");
			click(eleLogin6);
			Thread.sleep(5000);
			List<WebElement> store= driver.findElementsByXPath("(//div[@class='car-listing'])");
			int carSize = store.size();
			System.out.println(carSize);
			List<Integer> values= new ArrayList<Integer>();
			for (int i=1;i<=carSize;i++) {
				String text= driver.findElementByXPath("(//div[@class='car-listing']/div["+i+"])//div[@class='price']").getText();
				String replaceAll = text.replaceAll("\\D", "");
				int parseInt = Integer.parseInt(replaceAll);
				
				values.add(parseInt);

			}
			Integer max = Collections.max(values);
			String carValues = driver.findElementByXPath("(//div[@class="+max+"])/preceding::h3[1]").getText();
			System.out.println(carValues);
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}


	}

}




