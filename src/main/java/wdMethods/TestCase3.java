package wdMethods;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase3 extends ProjectMethods{

	@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "gopi";
		category = "smoke";
	}

	@Test(groups= {"regression"},dependsOnGroups= {"sanity"})
	public void createLeads() throws Exception {
		Thread.sleep(3000);
		WebElement cl = locateElement("linktext", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), "TL");
		type(locateElement("createLeadForm_firstName"), "Koushik");
		type(locateElement("createLeadForm_lastName"), "Chatterjee");
		click(locateElement("class", "smallSubmit"));
	}

}

