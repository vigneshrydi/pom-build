package wdMethods;



	
	import org.openqa.selenium.WebElement;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	
	import wdMethods.SeMethods;

	public class TC002EditLead extends ProjectMethods{

		@BeforeTest(groups= {"any"})
			public void setData() {
				testCaseName = "TC001_CreateLead";
				testDesc = "Create A new Lead";
				author = "gopi";
				category = "smoke";
			}
		@Test(groups= {"sanity"},dependsOnGroups={"smoke"})
		public void editLead() {
			
			WebElement cl = locateElement("linktext", "Create Lead");
			click(cl);
			type(locateElement("createLeadForm_companyName"), "TL");
			type(locateElement("createLeadForm_firstName"), "Koushik");
			type(locateElement("createLeadForm_lastName"), "Chatterjee");
			click(locateElement("class", "smallSubmit"));
		}
		
	}

