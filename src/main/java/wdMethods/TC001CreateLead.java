/*package wdMethods;

import java.io.IOException;

import org.junit.runners.Parameterized.UseParametersRunnerFactory;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import week9.day9.ReadExcel;

public class TC001CreateLead extends ProjectMethods{

	@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDesc = "Create A new Lead";
		author = "gopi";
		category = "smoke";
	}
    
	@Test(groups= {"smoke"},dataProvider="qa")
	public void createLead(String cName,String Fname,String Lname) throws Exception {
		Thread.sleep(3000);
		WebElement cl = locateElement("linktext", "Create Lead");
		click(cl);
		type(locateElement("createLeadForm_companyName"), cName);
		type(locateElement("createLeadForm_firstName"), Fname);
		type(locateElement("createLeadForm_lastName"), Lname);
		click(locateElement("class", "smallSubmit"));
	}
	
	
	@DataProvider(name="qa",indices= {1})
	public Object[][] fetchData() throws IOException{
		Object[][] read =ReadExcel.readExcel();
		return read;
		Object[][] data=new Object[2][3];
		data[0][0]="TestLeaf";
		data[0][1]="viki";
		data[0][2]="raja";
		
		data[1][0]="alpha";
		data[1][1]="karthi";
		data[1][2]="raj"
		//return data;
	}

}

*/