package wdMethods;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendHtmlReport extends ProjectMethods {
	@Test
	public void name() throws IOException {
	ExtentHtmlReporter html= new ExtentHtmlReporter("./reports/result.html");
	html.setAppendExisting(true);
	ExtentReports extend = new ExtentReports();
	extend.attachReporter(html);
	ExtentTest logger=extend.createTest("TC002__merge","create new lead");
	logger.assignAuthor("vignesh");
	logger.assignCategory("smoke");
logger.log(Status.PASS,"the data demo sucess",MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img1.png").build());
logger.log(Status.PASS,"the crmsf sucess",MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
logger.log(Status.PASS,"the login sucess",MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
extend.flush();
}
}
