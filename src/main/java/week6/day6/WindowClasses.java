package week6.day6;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowClasses {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByLinkText("AGENT LOGIN").click();
		driver.findElementByLinkText("Contact Us").click();
		Set<String> allWindows = driver.getWindowHandles();
		List<String> lst= new ArrayList<>();
		lst.addAll(allWindows);
		driver.switchTo().window(lst.get(1));
		File pic = driver.getScreenshotAs(OutputType.FILE);
		File obj= new File("./snap/img1.jpeg");
		FileUtils.copyFile(pic,obj);
		driver.switchTo().window(lst.get(0));
		driver.close();
		
	}
	

	
}
