package week6.day6;

import org.openqa.selenium.chrome.ChromeDriver;

public class AlertDemo {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("(//button[@onclick='myFunction()'])[1]").click();
		driver.switchTo().alert().sendKeys("vignesh");
		//driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		String compare = driver.findElementById("demo").getText();
		if(compare.contains("vignesh")){
			System.out.println("verified");
		}else
			System.out.println("not verified");
		}
		

	}


