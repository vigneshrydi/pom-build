package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class LoginPages extends ProjectMethods {
	public LoginPages() {
		PageFactory.initElements(driver, this);
		
	}
	// pagefactory @findby->similar to locate elememnt
	@FindBy(how=How.ID,using="username")
	WebElement eleuserName;
	@FindBy(how=How.ID,using="password")
	WebElement elepassword;
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
	WebElement elelogin;
	
	public LoginPages enterUserName( String uName) {
		type(eleuserName,uName);
		return this;
		
	}
   public LoginPages enterPassword(String Upassword) {
	   type(elepassword,Upassword);
	   return this;
		
	}
   public HomePage ClickLogin() {
	   click(elelogin);
	   return new HomePage();
		
	}
	
	

}
