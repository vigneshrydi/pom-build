package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public void MyLeCreateLeadads() {
		
		PageFactory.initElements(driver, this);
		
	}
@FindBy(how=How.NAME ,using="companyName")
WebElement elecompanyName;
@FindBy(how=How.NAME,using="firstName")
WebElement elefirstName;
@FindBy(how=How.NAME,using="lastName")
WebElement elastName;
@FindBy(how=How.CLASS_NAME,using="smallSubmit")
WebElement eleclick;


public CreateLead  entercompanyName1(String CName) {
	type(elecompanyName,CName);
	return this;
	
}
public CreateLead  enterfirstName(String Fname) {
	type(elefirstName,Fname);
	return this;
	
}
public CreateLead  entercompanyName(String Lname) {
	type(elastName,Lname);
	return this;
	
}public ViewLead  ClickCreateLead(String CClick) {
	type(eleclick,CClick);
	return new ViewLead();

}
}
