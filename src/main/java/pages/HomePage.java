package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods {
	public HomePage() {
		PageFactory.initElements(driver, this);
		
	}
	
	// pagefactory @findby->similar to locate elememnt
	@FindBy(how=How.LINK_TEXT,using="CRM/SFA")
	WebElement eleClick;
	
	
	@FindBy(how=How.CLASS_NAME,using="decorativeSubmit")
	WebElement elelogOut;
	
	
		
	
   public LoginPages ClickLogout() {
	   click(elelogOut);
	   return new LoginPages();
		
	}
   public MyHome ClickCrmsfa() {
	   click(eleClick);
	   return new MyHome();
   }
	
	

}
