package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods {
	public MergeLead() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID,using="ComboBox_partyIdFrom")
	WebElement eleFrmlead;
	@FindBy(how=How.ID,using="ComboBox_partyIdTo")
	WebElement eletolead;
	@FindBy(how=How.LINK_TEXT,using="Merge")
	WebElement elemerge;
	public MergeLead enterFromLead(String FromName) {
		type(eleFrmlead,FromName);
		return this;
		
	}
    public MergeLead enterToLead(String ToName) {
		type(eletolead,ToName);
		return this;
		
	}
   public ViewLead clickMerge() {
	   click(elemerge);
	   return new ViewLead();
	
}
}
