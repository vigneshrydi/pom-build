//Author
package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPages;
import wdMethods.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods {
	@BeforeTest
	public void setdata() {
		testCaseName="firstTestCase";
		testDesc="login into logout";
		category="smoke";
		author="vignesh";
		dataSheetName="sheet";
		
	}
	@Test(dataProvider="fetchData")
	public void loginLogout(String uName, String Upassword) {
		try {
			new LoginPages()
			.enterUserName(uName)
			.enterPassword(Upassword)
			.ClickLogin();
			//ClickLogout();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
